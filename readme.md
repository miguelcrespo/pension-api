# **PensionApp: Implementación de la API** #

Aplicación donde implementó tecnologías como PHP (Laravel), CSS (Bootstrap), Javascript(AngularJS, CoffeeScript), GIT, Yeoman, entre otros.

![Pension App.png](https://bitbucket.org/repo/4ogAAL/images/2603646586-Pension%20App.png)


La idea principal de esta iniciativa era crear un espacio donde los dueños de pensiones (casas arrendadas principalmente para estudiantes) pudieran publicar sus hogares y hacerlos accesible a todos los estudiantes de la Universidad del Magdalena, disponiendo de un sistema de calificación de usuarios para asi hacer mas facil el acceso a esta clase de informacion para los estudiantes, reducir la contaminacion visual de los avisos y ayudarle a los propietarios de estos hogares a conseguir huespedes de una manera mas rapida y sencilla.