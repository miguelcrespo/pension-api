<?php

/**
 * Created by Miguel.
 * User: miguel
 * Date: 3/12/14
 * Time: 11:22 PM
 */
class DistanceService
{
    private function deg2rad($deg)
    {
        return $deg * (pi() / 180);
    }
    public function distanceHaversine($origin, $destination)
    {


        $R = 6371;
        $dLat = $this->deg2rad($destination->latitude - $origin->latitude);
        $dLon = $this->deg2rad($destination->longitude - $origin->longitude);
        $a =
            sin($dLat / 2) * sin($dLat / 2) +
            cos($this->deg2rad($origin->latitude)) * cos($this->deg2rad($destination->latitude)) *
            sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $d = ($R * $c) * 1000;
        return $d;
    }
}
