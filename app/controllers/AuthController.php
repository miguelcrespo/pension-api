<?php
/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 16/10/14
 * Time: 12:42 PM
 */

use GuzzleHttp\Subscriber\Oauth\Oauth1;
/*
 * Controller to allow user login or sign up with email or password or Gmail account, Facebook account
 */

class AuthController extends BaseController
{

    /**
     * Login for users
     *
     * @param string $email
     * @param string $password
     * @return array with information about login transaction
     */
    public function login($email, $password)
    {
        $response = new ResponseController(microtime(true));

        $user = [
            "email" => $email,
            "password" => Hash::make($password)
        ];

        $validations = [
            'email' => 'required|email',
            'password' => ['required', 'min:5']
        ];

        $validator = Validator::make($user, $validations);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setError(true);
            $response->setDescription(json_decode($messages));
        } else {
            $user = User::where('email', '=', $email)->first();

            if (!$user) {
                return Response::json(array('message' => 'Wrong email and/or password'), 401);
            }
        }

        if (Hash::check($password, $user->password)) {
            // The passwords match...
            unset($user->password);
            $response->setError(false);
            $response->setDescription("login success ");
            $response->setData($user);
            $asa = $response->getResponse();
            $asa["token"] = $this->createToken($user);
            return $asa;
        }

        $response->setError(true);
        $response->setDescription("login error");


        return ($response->getResponse());

    }

    /**
     * Register an user into system
     *
     * @param string $name user's name
     * @param integer $email user's email
     * @param integer $password user's password
     * @return array with information about transaction
     */
    public function signUp($name, $email, $password)
    {
        $response = new ResponseController(microtime(true));

        $user = [
            "name" => $name,
            "email" => $email,
            "password" => Hash::make($password)
        ];

        $validations = [
            'name' => ['required', 'min:5'],
            'email' => ['required', 'email'],
            'password' => ['required', 'min:5']
        ];

        $validator = Validator::make($user, $validations);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setError(true);
            $response->setDescription(json_decode($messages));
        } else {
            $response->setError(false);
            $response->setDescription("user has been created");
            print_r(User::create($user));
        }
        return ($response->getResponse());

    }

    public function facebook()
    {
        $response = new ResponseController(microtime(true));
        $accessTokenUrl = 'https://graph.facebook.com/oauth/access_token';
        $graphApiUrl = 'https://graph.facebook.com/me';


        $params = array(
            'code' => Input::get('code'),
            'client_id' => Input::get('clientId'),
            'redirect_uri' => Input::get('redirectUri'),
            'client_secret' => 'fc5c6ac174f0fa31e8766b7c148e2247'
        );

        $client = new GuzzleHttp\Client();


        // Step 1. Exchange authorization code for access token.
        $accessTokenResponse = $client->get($accessTokenUrl, ['query' => $params]);

        $accessToken = array();
        parse_str($accessTokenResponse->getBody(), $accessToken);

        // Step 2. Retrieve profile information about the current user.
        $graphiApiResponse = $client->get($graphApiUrl, ['query' => $accessToken]);
        $profile = $graphiApiResponse->json();

        //print_r($profile);
        //$this->saveProfilePhoto($profile['id']);



        // Step 3a. If user is already signed in then link accounts.
        if (Request::header('Authorization')) {
            $user = User::where('facebook', '=', $profile['id']);

            if ($user->first()) {
                return Response::json(array('message' => 'There is already a Facebook account that belongs to you'), 409);
            }

            $token = explode(' ', Request::header('Authorization'))[1];
            $payloadObject = JWT::decode($token, 'secrets.TOKEN_SECRET');
            $payload = json_decode(json_encode($payloadObject), true);

            $user = User::find($payload['sub']);
            $user->facebook = $profile['id'];
            $user->displayName = $user->displayName || $profile['name'];
            $user->save();

            return Response::json(array('token' => $this->createToken($user)));
        } else {
            $user = User::where('facebook', '=', $profile['id']);
            if ($user->first()) {
                $user = $user->first();
                //return Response::json(array('token' => $this->createToken($user)));
            } else {
                $user = User::where('email', '=', $profile['email']);
                // link with existing account
                if ($user->first()) {
                    $user = $user->first();
                    $user->facebook = $profile['id'];
                    if ($user->gender == null) {
                        $user->gender = $profile['gender'];
                    }

                } else {
                    $user = new User;
                    $user->facebook = $profile['id'];
                    $user->name = $profile['name'];
                    $user->gender = $profile['gender'];
                    $user->email = $profile['email'];
                }

                $user->save();
            }
            $response->setError(true);
            $response->setDescription("");
            $response->setData($user);
            $asa = $response->getResponse();
            $asa["token"] = $this->createToken($user);

            return Response::json($asa);
        }


    }

    private function saveProfilePhoto($id)
    {
        $path = "/images";
        $profile_picture = file_get_contents("http://graph.facebook.com/" . $id . "/picture");
        $result = File::makeDirectory('/path/to/directory');
    }

}