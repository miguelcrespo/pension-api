<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 13/10/14
 * Time: 06:14 PM
 */
class PropertyController extends BaseController
{
    public function search($search)
    {
        $response = new ResponseController(microtime(true));
        //$city = City::where('name', 'LIKE', '%' . $search->place->city->short_name . '%')->first();
        require_once("DistanceService.php");

        $properties = Property::where('is_available', '=', true)->get();
        $distance_service = new DistanceService();
        foreach ($properties as $key => $property) {
            $origin = ["latitude" => $search->latitude, "longitude" => $search->longitude];
            $destination = ["latitude" => $property->latitude, "longitude" => $property->longitude];

            $distance = $distance_service->distanceHaversine((object)$origin, (object)$destination);
            if ($distance < $search->distance->max) {
                $property->distance = $distance;
                $property = $this->filterByAvailable($property, $search->type);

                if (count($property->rooms) > 0) {

                    //echo " KEY: ".$key;
                    //print_r($properties[$key]);
                    $properties[$key]->rooms = $property->rooms;

                    if ($search->advanced) {
                        $property = $this->filterByPrice($property, $search->type, $search->price);
                        if (count($property->rooms) > 0) {
                            $properties[$key] = $property;
                            $property = $this->filterByServices($property, $search->type, $search->services);
                            if (count($property->rooms) > 0) {
                                $properties[$key] = $property;
                            } else {
                                unset($properties[$key]);
                            }
                        } else {
                            unset($properties[$key]);
                            //$properties->values();
                        }
                    }
                } else {
                    unset($properties[$key]);
                    //$properties->values();
                }
            } else {
                unset($properties[$key]);
                //$properties->values();
            }
        }
        $properties->values();
        $response->setError(false);
        $response->setData($properties);
        return $response->getResponse();

    }


    private function filterByAvailable($property, $type)
    {
        foreach ($property->rooms as $key => $room) {
            if ($type == 1 and !$room->is_available) {
                unset($property->rooms[$key]);
                $property->rooms->values();
            }
        }
        return $property;

    }

    private function filterByPrice($property, $type, $price)
    {
        foreach ($property->rooms as $key => $room) {
            if ($room->type == 1 and $type == 1 and $room->price < $price->min or $room->price > $price->max) {
                //print_r($property->rooms);
                unset($property->rooms[$key]);
                $property->rooms->values();
            }
        }
        return $property;
    }

    private function filterByServices($property, $type, $services)
    {
        foreach ($property->rooms as $key => $room) {
            $delete = true;
            foreach ($services as $service) {
                // echo "Service: ".$service->name;
                foreach ($room->services as $service1) {
                    if ($service1->id == $service->id) {
                        // echo "EL servicio lo posee el cuarto";
                        $delete = false;
                        break;
                    }
                }
                if ($delete) {
                    //echo "EL servicio no lo posee el cuarto";
                    unset($property->rooms[$key]);
                    $delete = true;
                }

            }

        }

        $property->rooms->values();
        return $property;
    }
}
