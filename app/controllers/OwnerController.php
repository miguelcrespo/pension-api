<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 8/12/14
 * Time: 11:29 AM
 */
class OwnerController extends BaseController
{

    /**
     * Find and list user's properties
     *
     * @param int $user user's id
     * @return array with user's properties
     */
    public function myProperties($user)
    {
        $response = new ResponseController(microtime(true));
        if (is_object($user->owner)) {
            $propertiesarray = [];
            foreach ($user->owner->properties as $property) {

                $propertyobject = [
                    "id" => $property->id,
                    "name" => $property->name,
                    "owner_id" => $property->owner->id,
                    "address" => $property->address,
                    "latitude" => $property->latitude,
                    "longitude" => $property->longitude,
                    "iscertified" => $property->iscertified,
                    "type" => $property->type,
                    "cities_id" => $property->cities_id,
                    "rooms" => $this->getArrayRooms($user, $property->rooms)
                ];

                array_push($propertiesarray, $propertyobject);
            }
            $response->setError(false);
            $response->setData($propertiesarray);
        } else {
            $response->setError(false);
            $response->setDescription("User is not an owner");
            $response->setData([]);
        }
        return $response->getResponse();
    }

    /**
     * Pre-process to create a property
     *
     * @param int $userid user's id
     * @param string $name address of property
     * @param string $address address of property
     * @param string $latitude latitude and longitude of new property
     * @param string $longitude latitude and longitude of new property
     * @param int $type Type of lease of property (1 -> entire property, 2 -> by room, 3 -> by bed)
     * @param int $city city id
     */
    public function addProperty($property)
    {
        $response = new ResponseController(microtime(true));
        $user = Auth::user();
        if (!is_object($user->owner)) {
            $owner = new Owner();
            $owner->users_id = $user->id;
            $owner->save();
        }
        $propertyinserted = $this->createProperty($user, $property);
        if (is_object($propertyinserted)) {

            // Ver que reduelve este metodo
            $response->setError(false);
            # Add rooms to property
            $user = Auth::user();
            foreach ($property->rooms as $room) {
                $this->addRoomToProperty($user->id, $room, $propertyinserted->id);
            }

        } else {
            $response->setError($propertyinserted);
            $response->setDescription($propertyinserted);
        }
        return $response->getResponse();
    }

    private function createProperty($user, $property)
    {
        $property = [
            "name" => $property->name,
            "owner_id" => $user->owner->id,
            "address" => $property->address,
            "latitude" => $property->latitude,
            "longitude" => $property->longitude,
            "iscertified" => false,
            "type" => intval($property->type),
            "cities_id" => $property->city
        ];

        $validations = [
            'name' => ['required'],
            'owner_id' => ['required', 'integer'],
            'address' => ['required'],
            'latitude' => ['required'],
            'longitude' => ['required'],
            'type' => ['required', 'between:1,3']
        ];

        $validator = Validator::make($property, $validations);

        if ($validator->fails()) {
            return null;
        } else {
            return Property::create($property);
        }
    }

    public function addRoomToProperty($userid, $room, $property_id)
    {
        #echo "Price: ".$price." Property id: ".$property_id."<br>";
        $response = new ResponseController(microtime(true));
        $room2 = [
            'price' => $room->price,
            'users_id' => $userid,
            'properties_id' => $property_id,
            'isoccupied' => false
        ];

        $validations = [
            'price' => 'required|integer',
            'users_id' => 'required|integer',
            'properties_id' => 'required|integer|exists:properties,id'
        ];

        $validator = Validator::make($room2, $validations);

        if ($validator->fails()) {
            return $validator->messages();
        } else {
            //echo $property_id;
            $property = Property::find($property_id);
            // Check if property belong to active user
            if ($userid == $property->owner->user->id) {
                $roomdb = Room::create($room2);
                $response->setError(false);
                $response->setDescription("room successfully added");
                $response->setData($roomdb);

                $this->addServicestoRoom($roomdb, $room->services);
                return json_encode($response->getResponse());
            }

        }
        $response->setError(false);
        $response->setDescription("Room cannot be added");
        //$response->setData($property);
        return json_encode($response->getResponse());
    }

    public function addServicestoRoom($roomparameter, $services)
    {
        print_r($roomparameter);
        $room = Room::find($roomparameter->id);
        foreach ($services as $service) {
            $servicevalidator = [
                "id" => $service->id,
                "name" => $service->name,
                "ticked" => $service->ticked
            ];

            $validations = [
                'id' => 'required|integer|exists:services,id',
                'name' => 'required',
                'ticked' => 'required|boolean'
            ];

            $validator = Validator::make($servicevalidator, $validations);

            if ($validator->fails()) {
                return $validator->messages();
            } else {
                if ($service->ticked) {

                    $room->services()->attach($service->id);
                }
            }
        }

    }

    public function addBedToRoom($userid, $price, $roomid)
    {
        $response = new ResponseController(microtime(true));
        $bed = [
            'price' => $price,
            'rooms_id' => $roomid,
            'users_id' => $userid,
            'isoccupied' => false
        ];

        $validations = [
            'price' => 'required|integer',
            'rooms_id' => 'required|integer|exists:rooms,id',
            'users_id' => 'required|integer'
        ];

        $validator = Validator::make($bed, $validations);

        if ($validator->fails()) {
            $response->setError(true);
            $response->setDescription($validator->messages());
        } else {
            //echo $property_id;
            $room = Room::find($roomid);

            // Check if property belong to active user
            if ($userid == $room->property->owner->user->id) {
                $beddb = Bed::create($bed);
                $response->setError(false);
                $response->setDescription("bed successfully added");
                $response->setData($beddb);
                return json_encode($response->getResponse());
            }


        }
        //$response->setData($property);
        return json_encode($response->getResponse());
    }

    public function getArrayRooms($user, $rooms, $hide_occupied = false)
    {
        $return = [];
        foreach ($rooms as $room) {
            $roomobject = [];
            if ($room->type == 1) {
                if (($hide_occupied and !$room->isoccupied) or !$hide_occupied) {
                    $roomobject = [
                        "id" => $room->id,
                        "price" => $room->price,
                        "isoccupied" => $room->isoccupied,
                        "type" => $room->type,
                        "services" => $this->getServices($room)
                    ];
                }
                //$roomobject['requests'] = $this->getRequests($room);
            } else if ($room->type == 2) {
                $roomobject = [
                    "id" => $room->id,
                    "price" => $room->price,
                    "isoccupied" => $room->isoccupied,
                    "type" => $room->type,
                    "beds" => $this->getArrayBeds($user, $room->beds, 0)
                ];
            }
            if (count($roomobject) > 0) {

                $check = DB::select('select * from users_has_rooms where users_id = ? and rooms_id = ? and reservation_end = 000-00-00 and approved = 0', [$user->id, $room->id]);
                if (count($check) > 0) {
                    $roomobject["applied"] = "true";
                }
                array_push($return, $roomobject);
            }
        }
        return $return;
    }

    public function getArrayBeds($user, $beds, $option)
    {
        $return = [];
        foreach ($beds as $bed) {
            $bedobject = [
                "id" => $bed->id,
                "price" => $bed->price,
                "isoccupied" => $bed->isoccupied
            ];
            if ($option == 1) {
                $requests = $this->requestLease(3, $bed);
                $bedobject["requests"] = $requests;
            }

            $check = DB::select('select * from users_has_beds where users_id = ? and beds_id = ? and reservation_end = 000-00-00 and approved = 0 and active =1', [$user->id, $bed->id]);
            if (count($check) > 0) {
                $yourrequests = [];
                foreach ($check as $apply) {
                    $applyobject = [
                        "id" => $apply->id,
                        "approved" => $apply->approved,
                        "created_at" => $apply->created_at
                    ];
                    array_push($yourrequests, $applyobject);
                }
                $bedobject["yourrequest"] = $yourrequests;

            }
            array_push($return, $bedobject);
        }
        return $return;
    }


    public function approveRequest($userid, $request, $type)
    {
        $response = new ResponseController(microtime(true));
        $fields = [
            "request" => $request,
            "users_id" => $userid
        ];

        $validations = [
            'request' => 'required|integer',
            'users_id' => 'required|integer|exists:users,id',
        ];

        $validator = Validator::make($fields, $validations);
        if ($type == 3) {

            $result = DB::select('select * from users_has_beds where id = ? ', [$request]);
            $kind = Bed::find($result[0]->beds_id);

        } else if ($type == 2) {
            $result = DB::select('select * from users_has_rooms where id = ? ', [$request]);
            if (count($result) > 0) {
                $kind = Room::find($result[0]->rooms_id);
            }
        }

        if ($validator->fails() || count($result) == 0 || !$kind) {
            $response->setError(true);
            $response->setDescription($validator->messages());
        } else {
            $user = User::find($userid);

            // if bed type is same type lease
            if ($type == 3) { // by Bed
                if ($kind->room->property->type == 3 && !$kind->isoccupied) {
                    $owner = $kind->room->property->owner;
                    if ($owner->users_id == $user->id) {
                        $date = new DateTime();
                        $rowsupdated = DB::update('UPDATE users_has_beds SET approved = ?, reservation_date = ? WHERE id = ? ', [true, $date->format('Y-m-d H:i:s'), $request]);

                        if ($rowsupdated == 1) {
                            $kind->isoccupied = true;
                            $kind->save();
                        }

                        $response->setError(false);
                        $response->setDescription("approved : " . $rowsupdated);
                    } else {
                        $response->setError(true);
                        $response->setDescription("you are not the owner of this property");
                    }
                } else {
                    $response->setError(true);
                    $response->setDescription("Bed is not empty");
                }

            }
        }
        echo json_encode($response->getResponse());
    }

    private function getServices($room)
    {
        $services = [];
        foreach ($room->services as $service) {
            array_push($services, [
                "id" => $service->id,
                "name" => $service->name,
                "icon" => "http://pruebaspensiones.hol.es/public/images/services/" . $service->id . ".png"
            ]);
        }
        return $services;

    }

    public function getRequestsToMyProperties($user)
    {
        $requests = DB::select('select * from users_has_rooms where users_id = ? and reservation_end = 000-00-00 and approved = 0 and active =1', [$user->id]);
        $requests_rooms = [];
        foreach ($requests as $request) {

            $room = Room::find($request->rooms_id);
            $user = User::find($request->users_id);

            $request_array = [
                "id" => $request->id,
                "type" => 1,
                "user" => [
                    "id" => $user->id,
                    "name" => $user->name
                ],
                "room" => [
                    "id" => $room->id,
                    "description" => $room->description,
                    "property" => [
                        "id" => $room->property->id,
                        "name" => $room->property->name
                    ]
                ],
                "created_at" => $request->created_at
            ];
            array_push($requests_rooms, $request_array);
        }

        return $requests_rooms;

    }

    public function actionRequest($id, $type, $action, $user)
    {
        $response = new ResponseController(microtime(true));
        $date = new DateTime();
        if ($type == 1) {
            $query = DB::select('SELECT * from users_has_rooms where id = ?', [$id])[0];
            $room = Room::find($query->rooms_id);
            if ($room->property->owner->user->id != $user->id) {
                $response->setError(true);
            } else {
                if ($action) {
                    $update = DB::update('UPDATE users_has_rooms SET approved = ?, reservation_date = ? WHERE id = ? ', [true, $date->format('Y-m-d H:i:s'), $id]);
                    if ($update == 1) {
                        $this->deleteOtherRequestsExcept($id, $type);
                        $room->isoccupied = true;
                        $room->save();
                        $response->setError(false);
                    } else {
                        $response->setError(true);
                    }
                } else {
                    $update = DB::update('UPDATE users_has_rooms SET active =  WHERE id = ? ', [false, $id]);
                }
            }


        } else if ($type == 2) {

        }
        return json_encode($response->getResponse());
    }

    private function deleteOtherRequestsExcept($id, $type)
    {
        if ($type == 1) {
            $update = DB::delete('DELETE FROM users_has_rooms where not id = ? and approved = 0', [$id]);
        } else {
            $update = DB::delete('DELETE FROM users_has_rooms where not id = ? and approved = 0', [$id]);
        }
    }
}
