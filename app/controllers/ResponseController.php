<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 8/11/14
 * Time: 11:42 PM
 */
class ResponseController
{

    private $data = null;
    private $description;
    private $error = false;
    private $init = null;

    /**
     * Build of Response Controller
     *
     * @param date $date init date
     * @return array with user's properties
     */
    public function ResponseController($date){
        $this->init = $date;
    }

    public function setInit($init){
        $this->init = $init;
    }

    public function setError($error){
        $this->error = $error;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function setData($data){
        $this->data = $data;
    }


    public function getResponse()
    {
        $response = [
            'processing_time' => microtime(true) - $this->init,
            'error' => $this->error,
            'description' => $this->description,
            'data' => $this->data
        ];
        return $response;
    }
}