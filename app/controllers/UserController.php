<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 8/11/14
 * Time: 11:10 PM
 */
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class UserController extends BaseController
{

    /**
     * Reserve a bed by an user
     *
     * @param int $userid
     * @param int $bed id bed
     * @return array with information about transaction
     */
    public function bedReserve($userid, $bed)
    {
        $response = new ResponseController(microtime(true));
        $reserve = [
            "users_id" => $userid,
            "beds_id" => $bed,
            "approved" => false
        ];

        $validations = [
            'users_id' => 'required|integer|exists:users,id',
            'beds_id' => 'required|integer|exists:beds,id'
        ];

        $validator = Validator::make($reserve, $validations);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setError(true);
            $response->setDescription(json_decode($messages));
        } else {
            // 3 -> by beds only
            if (Bed::find($bed)->room->property->type == 3 && !Bed::find($bed)->isoocupied) {
                $date = new DateTime();
                // SQL Below gets reservations user that it does not answered or approved
                $result =
                    DB::
                    select('select * from users_has_beds where users_id = ? and beds_id = ? and created_at >= DATE_SUB(NOW(),INTERVAL 24 HOUR) and reservation_end is null', [$userid, $bed]);
                if (count($result) == 0) {
                    $reserve = User::find($userid)->beds()->attach($bed, ['created_at' => $date->format('Y-m-d H:i:s'), 'updated_at' => $date->format('Y-m-d H:i:s')]);
                    $response->setError(false);
                    $response->setDescription("Your request is pending approval");
                } else {
                    $response->setError(true);
                    $response->setDescription("you have already requested for this bed ");
                }

            } else {
                $response->setError(true);
                $response->setDescription("This property is only available by beds");
            }

        }
        return $response->getResponse();
    }

    public function me($user)
    {
        $ownercontroller = new OwnerController();
        $my_properties = $ownercontroller->myProperties($user);
        $response = new ResponseController(microtime(true));
        $userinformation = [
            "id" => $user->id,
            "name" => $user->name,
            "email" => $user->email,
            "gender" => $user->gender,
            "requestsByRoom" => $this->roomRequests($user),
            "properties" => $my_properties['data'],
            "requests" => $ownercontroller->getRequestsToMyProperties($user)
        ];
        $response->setData($userinformation);
        $response->setError(false);
        return $response->getResponse();
    }

    private function roomRequests($user)
    {
        $requests = [];
        foreach ($user->rooms as $room) {
            if (!$room->pivot->approved) {
                $request = [
                    "reservation_date" => $room->pivot->reservation_date,
                    "reservation_end" => $room->pivot->reservation_end,
                    "created_at" => $room->pivot->created_at,
                    "review" => $room->pivot->review,
                    "review_comment" => $room->pivot->review_comment,
                    "approved" => $room->pivot->approved,
                    "room" => [
                        "id" => $room->id,
                        "price" => $room->price,
                        "type" => $room->type,
                        "property" => [
                            "id" => $room->property->id,
                            "name" => $room->property->name,
                            "address" => $room->property->address
                        ]
                    ]
                ];
                array_push($requests, $request);
            }
        }
        return $requests;
    }

    public function getPropertiesByCity($userid, $city)
    {
        $response = new ResponseController(microtime(true));
        $reserve = [
            "users_id" => $userid,
            "city" => $city
        ];

        $validations = [
            'users_id' => 'required|integer|exists:users,id',
            'city' => 'required|integer|exists:cities,id'
        ];

        $validator = Validator::make($reserve, $validations);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setError(true);
            $response->setDescription(json_decode($messages));
        } else {
            $properties = Property::where("cities_id", "=", $city)->get();
            if (count($properties) > 0) {
                $propertiesarray = [];
                $ownercontroller = new OwnerController();
                foreach ($properties as $property) {
                    $propertyobject = [
                        "id" => $property->id,
                        "owner" => [
                            "id" => $property->owner->id,
                            "name" => $property->owner->user->name
                        ],
                        "name" => $property->name,
                        "address" => $property->address,
                        "latitude" => $property->latitude,
                        "longitude" => $property->longitude,
                        "iscertified" => $property->iscertified,
                        "type" => $property->type,
                        "city" => [
                            "id" => $property->cities_id,
                            "name" => $property->city->name
                        ],
                        "rooms" => $ownercontroller->getArrayRooms($property->rooms)
                    ];

                    $available = false;
                    foreach ($property->rooms as $room) {

                        if (!$room->isoccupied) {
                            $available = true;

                        }
                    }

                    if ($available) {
                        array_push($propertiesarray, $propertyobject);
                    }
                }

                $response->setError(false);
                $response->setData($propertiesarray);
            }

        }
        return ($response->getResponse());
    }

    public function getPropertyById($user, $id)
    {
        $response = new ResponseController(microtime(true));
        $reserve = [
            "users_id" => $user->id,
            "property_id" => $id
        ];

        $validations = [
            'users_id' => 'required|integer|exists:users,id',
            'property_id' => 'required|integer|exists:properties,id'
        ];

        $validator = Validator::make($reserve, $validations);
        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setError(true);
            $response->setDescription(json_decode($messages));
        } else {
            $property = Property::find($id);
            foreach ($property->rooms as $key => $room) {
                $room->services;
                if ($room->type == 1) {

                    if (!$room->is_available) {
                        unset($property->rooms[$key]);
                    } else {
                        $requests = DB::select('select * from users_has_rooms where users_id = ? and rooms_id = ?', [$user->id, $room->id]);
                        if (count($requests) > 0) {
                            $property->rooms[$key]->applied = true;
                        }
                        //print_r($requests);
                    }
                } elseif ($room->type == 2) {
                    // bed case
                }
            }
            $property->rooms->values();

            $property->rooms;
            $property->owner->user;
            $property->city->zone->country;
            $response->setError(false);
            $response->setData($property);
        }
        return ($response->getResponse());
    }

    public function getGeographicInformation()
    {
        $response = new ResponseController(microtime(true));
        $countries = Country::all();
        foreach ($countries as $key => $country) {
            foreach ($country->zones as $key2 => $zone) {
                foreach ($zone->cities as $key3 => $city) {

                }
            }
        }
        $response->setError(false);
        $response->setData($countries);
        return ($response->getResponse());
    }

    // Get reviews of properties
    private function getReviews($property)
    {
        $reviews = [];
        foreach ($property->users as $user) {
            if ($user->reservation_date != null && $user->review != 0) {
                $reviewobject = [
                    "review" => $user->review,
                    "review_comment" => $user->review_comment,
                    "user" => [
                        "id" => $user->id,
                        "name" => $user->name
                    ]
                ];
                array_push($reviews, $reviewobject);
            }
        }

        return $reviews;
    }

    public function roomReserve($id, $user)
    {
        $response = new ResponseController(microtime(true));
        $reserve = [
            "users_id" => $user->id,
            "rooms_id" => $id,
            "approved" => false
        ];

        $validations = [
            'users_id' => 'required|integer|exists:users,id',
            'rooms_id' => 'required|integer|exists:rooms,id'
        ];

        $validator = Validator::make($reserve, $validations);

        if ($validator->fails()) {
            $messages = $validator->messages();
            $response->setError(true);
            $response->setDescription(json_decode($messages));
        } else {
            $room = Room::find($id);
            if ($room->type == 1 and $room->is_available) {
                #Check if user doesn't have any request to this room
                $find = false;
                foreach ($room->users as $request) {
                    if ($request->pivot->users_id == $user->id and (strtotime($request->pivot->reservation_end) == 0)) {
                        $response->setError(true);
                        $response->setDescription("You already have a request for this room");
                        $find = true;
                    }
                }
                if (!$find) {
                    if ($room->property->owner->users_id != $user->id) {
                        $date = new DateTime();
                        $room->users()->attach($user->id, ["created_at" => $date->format('Y-m-d H:i:s'), "updated_at" => $date->format('Y-m-d H:i:s')]);
                        $response->setError(false);
                    } else {
                        $response->setError(true);
                        $response->setDescription("No puedes aplicar a tu propia propiedad");
                    }
                }


            } else {

                $response->setDescription("This room is occupied or does not allow lease by room");
                $response->setError(true);
            }
        }
        return $response->getResponse();
    }
}
