<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('email', 150)->unique();
            $table->string('password', 120);
            $table->string('telephone')->nullable();
            $table->string('facebook', 200)->nullable();
            $table->string('google', 200)->nullable();
            $table->string('twitter', 200)->nullable();
            $table->string('gender', 15)->nullable();
            $table->timestamps();
        });

        Schema::create('owner', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->timestamps();
        });

        Schema::create('zones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('countries_id')->unsigned();
            $table->foreign('countries_id')->references('id')->on('countries');
            $table->timestamps();
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->integer('zones_id')->unsigned();
            $table->foreign('zones_id')->references('id')->on('zones');
            $table->timestamps();
        });

        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->timestamps();
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 155)->nullable();
            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')->references('id')->on('owner');
            $table->string('address', 155);
            $table->string('latitude', 45);
            $table->string('longitude', 45);
            $table->boolean('is_certified');
            $table->integer('cities_id')->unsigned();
            $table->foreign('cities_id')->references('id')->on('cities');
            $table->string('description', 200)->nullable();
            $table->string('how_to_get', 200)->nullable();
            $table->boolean('is_available')->nullable();
            $table->timestamps();
        });


        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('properties_id')->unsigned();
            $table->foreign('properties_id')->references('id')->on('properties');
            $table->float('price');
            $table->boolean('is_available');
            $table->integer('type');
            $table->string('description', 155)->nullable();
            $table->timestamps();
        });

        Schema::create('beds', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price');
            $table->boolean('is_available');
            $table->integer('rooms_id')->unsigned();
            $table->foreign('rooms_id')->references('id')->on('rooms');
            $table->timestamps();
        });

        Schema::create('rooms_has_services', function (Blueprint $table) {
            $table->integer('rooms_id')->unsigned();
            $table->integer('services_id')->unsigned();
            $table->foreign('rooms_id')->references('id')->on('rooms');
            $table->foreign('services_id')->references('id')->on('services');
            $table->primary(["rooms_id", 'services_id']);
            $table->string('description', 45)->nullable();
        });

        Schema::create('users_has_beds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('beds_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('beds_id')->references('id')->on('beds');
            $table->date('reservation_date')->nullable();
            $table->date('reservation_end')->nullable();
            $table->integer('review');
            $table->string('review_comment', 155)->nullable();
            $table->boolean('is_approved');
            $table->timestamps();
        });

        Schema::create('users_has_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id')->unsigned();
            $table->integer('rooms_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('rooms_id')->references('id')->on('rooms');
            $table->date('reservation_date');
            $table->date('reservation_end');
            $table->integer('review');
            $table->string('review_comment', 155)->nullable();
            $table->boolean('is_approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('users_has_rooms');
        Schema::drop('users_has_beds');
        Schema::drop('rooms_has_services');
        Schema::drop('beds');
        Schema::drop('rooms');
        Schema::drop('properties');
        Schema::drop('services');
        Schema::drop('cities');
        Schema::drop('zones');
        Schema::drop('countries');
        Schema::drop('owner');
        Schema::drop('users');
    }

}
