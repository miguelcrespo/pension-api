<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();
        $me = null;
        foreach (range(1, 20) as $index) {
            $me = User::create([
                'name' => $faker->name,
                'password' => '$2y$10$x6P29A3Bhmg2a07NNiwa6.UKuoT6gHKNKUXmKKVuSbcDy2S7PkHi.',
                'email' => $faker->email
            ]);
        }
        $me->name = "Miguel Crespo";
        $me->email = "miguel@gmail.com";
        $me->save();

        Owner::create([
            'users_id' => $me->id
        ]);

        $country = Country::create([
            'name' => 'Colombia'
        ]);
        $zone = Zone::create([
            'name' => 'Magdalena',
            'countries_id' => $country->id
        ]);

        $city = City::create([
            'name' => 'Santa Marta',
            'zones_id' => $zone->id
        ]);

        $property = null;
        foreach (range(1, 2) as $index) {
            $property = Property::create([
                'name' => $faker->word,
                'owner_id' => $me->owner->id,
                'address' => $faker->address,
                'latitude' => 11.227328,
                'longitude' => -74.205161,
                'cities_id' => $city->id
            ]);
        }

        /* $property->latitude = 11.227328;
         $property->longitude = -74.205161;
         $property->save();*/

        $room = Room::create([
            'properties_id' => $property->id,
            'price' => 250000,
            'is_available' => true,
            'type' => 1
        ]);

        Room::create([
            'properties_id' => $property->id,
            'price' => 500000,
            'is_available' => true,
            'type' => 1
        ]);

        Service::create([
            'name' => 'WiFi'
        ]);

        Service::create([
            'name' => 'Air'
        ]);

    }

}