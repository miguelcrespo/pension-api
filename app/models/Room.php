<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 8/13/14
 * Time: 3:21 PM
 */

class Room extends Eloquent {
    protected $table = 'rooms';
    protected $fillable = array('properties_id', 'price', 'is_available', 'type', 'description');

    public function property(){
        return $this->belongsTo('Property', 'properties_id');
    }

    public function beds(){
        return $this->hasMany('Bed', 'rooms_id');
    }

    public function services()
    {
        return $this->belongsToMany('Service', 'rooms_has_services', 'rooms_id', 'services_id')->withPivot('description');
    }

    public function users()
    {
        return $this->belongsToMany('User', 'users_has_rooms', 'rooms_id', 'users_id')->withPivot('reservation_date', 'reservation_end', 'review', 'review_comment');
    }
}