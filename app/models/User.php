<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
    protected $hidden = array('password', 'remember_token', 'facebook', 'google', 'twitter', 'gender');


    protected $fillable = array('name', 'email', 'password');

    public function getAuthIdentifier(){
        return $this->getKey();
    }

    public function getAuthPassword(){
        return $this->password;
    }

    public function getReminderEmail(){
        return $this->email;
    }

    public function owner(){
        return $this->hasOne('Owner', 'users_id');
    }

    public function beds(){
        return $this->belongsToMany('Bed', 'users_has_beds', 'users_id', 'beds_id')->withPivot('reservation_date', 'reservation_end', 'review', 'review_comment');
    }

    public function rooms()
    {
        return $this->belongsToMany('Room', 'users_has_rooms', 'users_id', 'rooms_id')->withPivot('reservation_date', 'reservation_end', 'review', 'review_comment');
    }



}
