<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 8/13/14
 * Time: 3:28 PM
 */

class Service extends Eloquent {
    protected $table = 'services';
    protected $fillable = array('name');

    public function rooms()
    {
        return $this->belongsToMany('Room', 'rooms_has_services', 'rooms_id', 'services_id')->withPivot('description');
    }
}