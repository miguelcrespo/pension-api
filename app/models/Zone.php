<?php
/**
 * Created by PhpStorm.
 * User: labso
 * Date: 14/08/14
 * Time: 06:25 PM
 */

class Zone extends Eloquent {
    protected $table = 'zones';
    protected $fillable = array('name');

    public function country(){
        return $this->belongsTo('Country', 'countries_id');
    }

    public function cities(){
        return $this->hasMany('City', 'zones_id');
    }

}