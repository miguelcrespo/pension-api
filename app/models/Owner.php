<?php
/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 8/12/14
 * Time: 11:24 AM
 */

class Owner extends Eloquent {
    protected $table = 'owner';
    protected $fillable = array('users_id');

    public function user(){
        return $this->belongsTo('User', 'users_id');
    }

    public function properties(){
        return $this->hasMany('Property', 'owner_id');
    }
}