<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 8/13/14
 * Time: 3:23 PM
 */

class Bed extends Eloquent {
    protected $table = 'beds';
    protected $fillable = array('price', 'isoccupied', 'rooms_id');

    public function room(){
        return $this->belongsTo('Room', 'rooms_id');
    }
    public function users(){
        return $this->belongsToMany('User', 'users_has_beds', 'beds_id', 'users_id')->withPivot('reservation_date', 'reservation_end', 'review', 'review_comment', 'approved');
    }
}