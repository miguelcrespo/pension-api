<?php
/**
 * Created by PhpStorm.
 * User: labso
 * Date: 14/08/14
 * Time: 06:25 PM
 */

class City extends Eloquent {
    protected $table = 'cities';
    protected $fillable = array('name', 'latitude', 'longitude');

    public function Zone(){
        return $this->belongsTo('Zone', 'zones_id');
    }

    public function properties(){
        return $this->hasMany('Property', 'cities_id');
    }
}