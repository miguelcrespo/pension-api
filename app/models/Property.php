<?php

/**
 * Created by PhpStorm.
 * User: miguel
 * Date: 8/12/14
 * Time: 12:53 AM
 */
class Property extends Eloquent
{
    protected $table = 'properties';
    protected $fillable = array('name', 'owner_id', 'address', 'latitude', 'longitude', 'iscertified', 'cities_id', 'description', 'howtoget', 'is_available');

    public function owner()
    {
        return $this->belongsTo('Owner', 'owner_id');
    }

    public function rooms()
    {
        return $this->hasMany('Room', 'properties_id');
    }

    public function city()
    {
        return $this->belongsTo('City', 'cities_id');
    }

    public function users()
    {
        return $this->belongsToMany('User', 'users_has_beds', 'beds_id', 'users_id')->withPivot('reservation_date', 'reservation_end', 'review', 'review_comment', 'approved');
    }
}
