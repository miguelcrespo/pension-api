<?php
/**
 * Created by PhpStorm.
 * User: labso
 * Date: 14/08/14
 * Time: 06:24 PM
 */
class Country extends Eloquent {
    protected $table = 'countries';
    protected $fillable = array('name');

    public function zones(){
        return $this->hasMany('Zone', 'countries_id');
    }
}