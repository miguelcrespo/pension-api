<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('hello');
});

/*
 * Route for register an user into system
 *
 * @return json with information about user's register
 * */
Route::any('signup', function () {

    $name = Input::get("name");
    $email = Input::get("email");
    $password = Input::get("password");

    $usercontroller = new AuthController();
    return json_encode($usercontroller->signUp($name, $email, $password));

});


/*
 * Route for login an user into system
 *
 * @return json with information about user's login
 * */
Route::any('islogged', function () {


    $responsecontroller = new ResponseController(microtime(true));

    $user = Auth::user();
    if (is_object($user)) {
        $responsecontroller->setData(true);
    } else {
        $responsecontroller->setData(false);
    }
    $responsecontroller->setError(false);
    return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));
});

Route::any('auth/login', function () {
    $email = Input::get("email");
    $password = Input::get("password");
    $authcontroller = new AuthController();
    return Response::json($authcontroller->login($email, $password))->setCallback(Input::get('callback'));
});

Route::any('auth/signup', function () {
    $name = Input::get("name");
    $email = Input::get("email");
    $password = Input::get("password");
    $authcontroller = new AuthController();
    return Response::json($authcontroller->signUp($name, $email, $password))->setCallback(Input::get('callback'));
});


Route::any('logout', function () {
    Auth::logout();
    $responsecontroller = new ResponseController(microtime(true));
    $responsecontroller->setError(false);


    return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

});
Route::post('auth/facebook', 'AuthController@facebook');
/*
 * Route group for user's transactions
 * */
Route::get('dbmigrate', 'DbmigrateController@index');

Route::any('geographicinformation', function () {
    $usercontroller = new UserController();
    return Response::json($usercontroller->getGeographicInformation())->setCallback(Input::get('callback'));
});
Route::group(array('before' => 'auth'), function () {

    Route::any('/upload/property-images', function () {
        //$filename = $_FILES['file']['name'];
        $responsecontroller = new ResponseController(microtime(true));

        $ext = explode(".", $_FILES['file']['name']);
        if ($ext[1] != null) {
            $filename = null;
            while (true) {
                $filename = uniqid('property_temp', true) . '.' . $ext[1];
                //echo "FileName: " . $filename;
                if (!file_exists(sys_get_temp_dir() . $filename)) break;
            }
            $destination = sys_get_temp_dir() . "/" . $filename;
            move_uploaded_file($_FILES['file']['tmp_name'], $destination);
            $responsecontroller->setError(false);
            $responsecontroller->setData(["filename" => $filename]);

        } else {
            $responsecontroller->setError(true);
        }

        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));

        //$filename = uniqid('temp', true) . '.png';

        //$destination = '../public/images/properties/temp' . $filename;
        //move_uploaded_file($_FILES['file']['tmp_name'], $destination);
        // $usercontroller = new UserController();


        //return Response::json($usercontroller->uploadImageForProperty($id, getUser()))->setCallback(Input::get('callback'));
    });
    # New routes for 2.0

    Route::any('searchproperties', function () {
        $search = Input::get('search');
        $search = json_decode($search);
        $propertycontroller = new PropertyController();

        return Response::json($propertycontroller->search($search))->setCallback(Input::get('callback'));
    });

    Route::any('roomreserve', function () {
        $id = Input::get('id');
        $usercontroller = new UserController();


        return Response::json($usercontroller->roomReserve($id, getUser()))->setCallback(Input::get('callback'));
    });


    Route::any('me', function () {
        $usercontroller = new UserController();
        return Response::json($usercontroller->me(getUser()))->setCallback(Input::get('callback'));
    });

    Route::post('getservices', function () {
        $url = "http://localhost/pension/public/images/services/";
        $responsecontroller = new ResponseController(microtime(true));
        $services = Service::All();
        $servicesresponse = [];
        foreach ($services as $service) {
            $servicearray = [
                "id" => $service->id,
                "name" => $service->name,
                "icon" => "<img src='" . $url . $service->id . ".png'/>"
            ];
            array_push($servicesresponse, $servicearray);
        }
        $responsecontroller->setData($servicesresponse);
        $responsecontroller->setError(false);


        return Response::json($responsecontroller->getResponse())->setCallback(Input::get('callback'));
    });

    Route::any('addproperty', function () {
        $property = json_decode(Input::get('property'));
        $ownercontroller = new OwnerController();
        return Response::json($ownercontroller->addProperty($property))->setCallback(Input::get('callback'));
    });

    Route::any('addroomtoproperty', function () {
        $ownercontroller = new OwnerController();
        $userid = Auth::user()->id;
        $price = Input::get('price');
        $property_id = Input::get('property');
        return Response::json($ownercontroller->addRoomToProperty($userid, $price, $property_id))->setCallback(Input::get('callback'));
    });

    Route::any('addbedtoroom', function () {
        $ownercontroller = new OwnerController();
        $userid = Auth::user()->id;
        $price = Input::get('price');
        $property_id = Input::get('room');
        echo $ownercontroller->addBedToRoom($userid, $price, $property_id);
    });

    Route::any('bedreserve', function () {
        $usercontroller = new UserController();
        $userid = Auth::user()->id;
        $bed = Input::get('bed');
        return Response::json($usercontroller->bedReserve($userid, $bed))->setCallback(Input::get('callback'));
    });

    Route::any('getrequestslease', function () {
        $ownercontroller = new OwnerController();
        $userid = Auth::user()->id;
        echo $ownercontroller->getRequestsLease($userid);
    });

    Route::any('approverequest', function () {
        $ownercontroller = new OwnerController();
        $userid = Auth::user()->id;
        $request = Input::get("request");
        $type = Input::get("type");
        $ownercontroller->approveRequest($userid, $request, $type);
        //echo $request;
    });


    Route::any('properties', function () {
        $usercontroller = new UserController();
        $userid = Auth::user()->id;
        $city = Input::get("city");
        return Response::json($usercontroller->getPropertiesByCity($userid, $city))->setCallback(Input::get('callback'));
    });

    Route::any('property', function () {
        $usercontroller = new UserController();

        $id = Input::get("id");
        return Response::json($usercontroller->getPropertyById(getUser(), $id))->setCallback(Input::get('callback'));
    });

    function getUser()
    {
        $token = explode(' ', Request::header('Authorization'))[1];
        $payloadObject = JWT::decode($token, 'secrets.TOKEN_SECRET');
        $payload = json_decode(json_encode($payloadObject), true);

        $userid = $payload['sub'];
        return User::find($userid);

    }

    /* Accept or reject a request*/
    Route::post('actionRequest', function () {
        $ownercontroller = new OwnerController();

        $id_request = Input::get("id");
        $type_request = Input::get("type");
        $action_request = Input::get("action");
        return Response::json($ownercontroller->actionRequest($id_request, $type_request, $action_request, getUser()))->setCallback(Input::get('callback'));
    });
});